import discord
from discord.ext import commands
from typing import List
from discord.message import Attachment
from discord.abc import User
from discord.channel import TextChannel


def command_prefix(bot, message):
    prefixes = [">", '<@!%s> ' % bot.user.id]
    if isinstance(message.channel, discord.DMChannel):
        prefixes.append("")
    return prefixes

bot = commands.Bot(command_prefix=command_prefix)
commands.when_mentioned_or('!')
class Greetings(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.Cog.listener()
    async def on_member_join(self, member):
        channel = member.guild.system_channel
        if channel is not None:
            await channel.send('Welcome {0.mention}.'.format(member))

    @commands.command()
    async def auction(self, ctx, *, member=None):
        """Card: 
Type of Auction:(Card for card, highest # of base or $ auction)
Time:

        """
        print(ctx)


    @commands.command()
    async def hello(self, ctx, *, member: discord.Member = None):
        """Says hello"""
        member = member or ctx.author
        if self._last_member is None or self._last_member.id != member.id:
            await ctx.send('Hello {0.name}~'.format(member))
        else:
            await ctx.send('Hello {0.name}... This feels familiar.'.format(member))
        self._last_member = member


bot.add_cog(Greetings(bot))


bot.run('NzY1NjQ1NjYxMDUxMDI3NTQ2.X4X1Fg.LQqvAtf_L6JeCP9t5Yzebe_1Ab0')


@dataclasss 
class Auction(object):
    cards: List[Attachment]
    owner: User
    channel: TextChannel
    